# Microcontroller
*presentation link
https://toddzebert.github.io/microcontroller-architecture-embedded-programming/#/*

## what is microcontroller
MCUs (or uC)
processor with some RAM
slower, simpler, less expensive, generally don't run OS (somethimes MTOS)
- Harvard architecture
- Von-Neuman architecture

some MCUs offer protected address space, privileged execution

### program memory
non-volatile
now, flash/EEPROM
16kb - 128 kb most common

### data memory

### instruction clock
40Mhz to 1GH, more frequently
16Mhz - 0.0625us
some can run multiple instructions on same cycle
most internal clocks are not accurate - get external crystal if care

### instruction sets
limited or less-complex

### data width

### cores
mostly single, some double to 8
other have some variations like multi-context, or real-time units

### math
add, subtract, (multiplicationi maybe), (division less common)
look-up table, approximation etc
"mod" is expensive
floating point math is rare. one technique

### Endianness
multi-byte data
little/big endian

### physical 
#### IC DIE
die is piece of silicon wafer contains the transistors and circuits
eg atmel attiny 13a

#### package
what holds the die
pic 10f...
attiny 85, 8 pin dip
TI TM s570 ARM cortex - large

#### variations
- system-on-chip (SoC) - everything on 1 die, side by side
- multi-chip moduels (MCM): modules (die)) known as chiplets
- systems-in-package: stack vertially to save space
- system-on-oackate (SoP); includes discrete components. possibly only needs to connect io and power and good to go
- customizable system-on-chip (cSoC) - includes fpga

exmaple: TI OSD335x SiP contains ARM Cortex MPU, ARM Cortex-M3 MCU, 2x PRU (programmable real-time units) in a BGA (ball grdi array) package

chiplets (reusable functional circuit blocks) used in MCM, SiP, SoP
can be stacked together
downsides: compatibility between chiplets

Open compute Project (OCP) wants to standandize chiplets
linux foundation started chips alliance
Intel
zGlue: MCM chip in small batches


### peripherals
interface on the die itself
runs independently as the code cycle

examples of peripherals
ADC
SPI
I^2C/IIC
I^2S/IIS
USB
Internet/wifi
n-bit timer/counter
CAN (controller area network)
real time clock
genral purpose io
uart
watchdog timer
1-wire

more expensive/larger --> more peripherals
MCUs have 6 - 500 pins

access to any peripheral maybe limited to only 1 pin and shared with other peripherals
something to flexibly assign
if necessary peripheal not exist, "bit banging" can be used to create the interface. interrupt system GPIO. likely mess up timing.

no file system. everything is memory mapped

example pinout
each pin has multiple functions
program to flip bits to tell microcontroller which one
micros exist. can use c-like language to manipulate

ATtiny 828: more complex
TI LM4F232H5QC


### power management
1 or more power states, depends on vendor
can get to real low tens of nA (sleep)
some peripherals or subsystem need to draw power like pin pull-up
you can pull-up or pull-down pins, to set to 1 or 0. if you don't, it is in in-determined state. when you try to read it, it could be anything


## program
USB to TTL serial adapter
development boards (arduino..)
ISCP: in-circut serial programing or in system programming (ISP)
JTAG connector, program/debug code

vendor specific
1 bit... (open source)
adafruit

### debug
breakpoint, step, step-over
writing to serial, toggling LED
--> all take system cycle, creates "heisenbugs"

### interrupts
interrupt service routine, interrupt handler code
interrupt priority, interrupt inside interrupt
SW generated by user code, overflows, division by zero
HW generated by peripherals, GIPO edge etc
this is standard - learn it

### RTOS
real time operating system
similar to OS, try to to deterministic

### configuration bits
fuses, fuse bits
change: clock speed
watchdog enabled
brown-out reset
jtag enabled
debug enabled
...

### notable MCUs
common: microship (was Atmel)
ATtiny
ATmega
ARM
PIC
TI MSP
AM335
ESP8266
RISC-V - open source
SiGive - open source
....


## FPGAs
field programmable gate array
unique type of IC, can reprogram to change how processor route data from logic block to logic block
in comparison to GPPs, ASICs

use:
- dev leading to ASICs
- alternative when MCU doesn't work well

### programming
HDL
it doesn't compile/interprete, it defines the logic block to be executed
can be run in simulator, verify in math

example: verilog
all code happens same time unless you tell it not to

#### toolchains
symbiFLow: gcc of fpgas (icestorm)
open dev boards: ice-breaker etc
some processor cores are avaiable in HDL so that FPGA can conain a MCU/CPU as a soft core
step 1 program to fpga
step 2 put you exe on it

pros: massively parallel, flexible, reconfigurable
cons: slower, more time consuming, more expensive (not that much)

### FPGA hybrids
fpga with "hard block" MCU cores
MCU comes with configurable

thingiverse.com/ToddZebert
instructables.com/member/ToddZebert
hackaday.io/toddzebert
medium.com/@toddzebert

